package com.tmorton.StockInvetory.controller;

import com.tmorton.StockInvetory.domain.StockInventory;
import com.tmorton.StockInvetory.service.StockInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class
StockWebController {
    @Autowired
    private StockInventoryService stockInventoryService;

    private static final String STOCK_TEMPLATE = "stock";
    private static final String STOCK_TEMPLATE_ID = "stock";
    private static final String HOMEPAGE_REDIRECT = "redirect:/";
    private static final String NEW_STOCK_TEMPLATE_ID = "newItem";
    private static final String STOCK_FORM_HEADER_ID = "formHeader";


    @GetMapping ("/")
    public String displayStock (Model model) {
        model.addAttribute (STOCK_FORM_HEADER_ID, "Add a new Item");
        model.addAttribute (STOCK_TEMPLATE_ID, this.stockInventoryService.findAllStock ());
        model.addAttribute (NEW_STOCK_TEMPLATE_ID, new StockInventory());
        return STOCK_TEMPLATE;
    }

    @GetMapping ("/delete/{id}")
    public String deleteItem (@PathVariable Integer id) {
        this.stockInventoryService.deleteStockInventoryById (id);
        return HOMEPAGE_REDIRECT;
    }

    @PostMapping ("/")
    public String addItem (Model model,
                              @Valid @ModelAttribute (NEW_STOCK_TEMPLATE_ID)
                                      StockInventory newItem,
                                      BindingResult bindingResult) {
        if (!bindingResult.hasErrors ()) {
            this.stockInventoryService.save (newItem);
            return HOMEPAGE_REDIRECT;
        }
        else {
            model.addAttribute (STOCK_FORM_HEADER_ID, "Please Correct the Cost");
            model.addAttribute (STOCK_TEMPLATE_ID, this.stockInventoryService.findAllStock ());
            return STOCK_TEMPLATE;
        }
    }

    @GetMapping ("update/{id}")
    public String editItem (Model model, @PathVariable Integer id) {
        model.addAttribute (STOCK_TEMPLATE_ID, this.stockInventoryService.findAllStock ());
        model.addAttribute (STOCK_FORM_HEADER_ID, "Please Change the Cost");
        model.addAttribute (NEW_STOCK_TEMPLATE_ID, this.stockInventoryService.findOne (id));
        return STOCK_TEMPLATE;
    }

    @PostMapping ("update/{id}")
    public String saveItem (Model model,
                               @PathVariable Integer id,
                               @Valid @ModelAttribute (NEW_STOCK_TEMPLATE_ID) StockInventory newItem,
                               BindingResult bindingResult) {
        if (!bindingResult.hasErrors ()) {
            StockInventory current = this.stockInventoryService.findOne (id);
            current.setName (newItem.getName ());
            current.setCost (newItem.getCost ());
            this.stockInventoryService.save (current);
            return HOMEPAGE_REDIRECT;
        }
        else {
            model.addAttribute (STOCK_FORM_HEADER_ID, "Please Correct the Comment");
            model.addAttribute (STOCK_TEMPLATE_ID, this.stockInventoryService.findAllStock ());
            return STOCK_TEMPLATE;
        }
    }
}

