package com.tmorton.StockInvetory.controller;

import com.tmorton.StockInvetory.domain.StockInventory;
import com.tmorton.StockInvetory.service.StockInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping ("/api")
@RestController
public class StockController {
    @Autowired
    private StockInventoryService stockInventoryService;

    @GetMapping ("/stock")
    public List <StockInventory> getAllStock () {
        return stockInventoryService.findAllStock ();
    }

    @GetMapping ("/stock/{id}")
    public StockInventory findStockInventoryById (@PathVariable("stock_id") Integer id) {
        return this.stockInventoryService.findStockInventoryById (id);
    }

    @GetMapping ("/name/{name}")
    public List <StockInventory> findStockInventoryByName (@PathVariable ("name") String name) {
        return this.stockInventoryService.findStockInventoryByName (name);
    }

    @DeleteMapping("/stock/{id}")
    public void deleteStockInventoryById (@PathVariable ("stock_id") Integer id) {
        this.stockInventoryService.deleteStockInventoryById (id);
    }

    @PostMapping("/add")
    public void addStock (@RequestBody StockInventory stockInventory) {
        this.stockInventoryService.save (stockInventory);
    }

    @PostMapping ("/update")
    public void updateStock (@RequestBody StockInventory stockInventory) {
        this.stockInventoryService.save (stockInventory);
    }
}

