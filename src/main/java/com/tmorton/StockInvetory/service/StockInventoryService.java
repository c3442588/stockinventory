package com.tmorton.StockInvetory.service;

import com.tmorton.StockInvetory.domain.StockInventory;
import com.tmorton.StockInvetory.domain.StockInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockInventoryService {
    @Autowired
    private StockInventoryRepository stockInventoryRepository;

    public List<StockInventory> findAllStock () {
        return this.stockInventoryRepository.findAll ();
    }

    public StockInventory findStockInventoryById (Integer id) {
        return this.stockInventoryRepository.findStockInventoryById(id);
    }

    public List <StockInventory> findStockInventoryByName (String name) {
        return this.stockInventoryRepository.findStockInventoryByName (name);
    }

    public void deleteStockInventoryById (Integer id) {
        this.stockInventoryRepository.delete (id);
    }

    public void save (StockInventory newItem) {
        this.stockInventoryRepository.save (newItem);
    }

    public StockInventory findOne (Integer id) {
        return this.stockInventoryRepository.findOne (id);
    }

}
