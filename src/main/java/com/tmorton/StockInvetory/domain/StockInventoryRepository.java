package com.tmorton.StockInvetory.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StockInventoryRepository extends CrudRepository<StockInventory, Integer> {
    @Override
    List<StockInventory> findAll ();

    StockInventory findStockInventoryById(Integer id);

    List<StockInventory> findStockInventoryByName (String name);
}
