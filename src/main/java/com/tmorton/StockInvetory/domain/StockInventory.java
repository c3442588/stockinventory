package com.tmorton.StockInvetory.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name = "stock")
public class StockInventory {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (name = "id")
    private Integer id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String cost;

    public StockInventory() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "StockInventory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expdate='" + cost + '\'' +
                '}';
    }
}
